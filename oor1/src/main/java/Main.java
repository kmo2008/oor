import etm.core.configuration.BasicEtmConfigurator;
import etm.core.configuration.EtmManager;
import etm.core.monitor.EtmMonitor;
import etm.core.monitor.EtmPoint;
import etm.core.renderer.SimpleTextRenderer;
import etm.core.timer.SunHighResTimer;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/**
 * Author: Kamil Ostrowski
 * Group: 155IC
 */
public class Main {
    private static final EtmMonitor etmMonitor = EtmManager.getEtmMonitor();

    public static void main(String[] args) {
        BasicEtmConfigurator.configure(true, new SunHighResTimer());
        etmMonitor.start();
        sequenceText();
        parallelText();
        fibbonaciSequence(15);
        Fib f = new Fib(15);
        EtmPoint point = etmMonitor.createPoint("Fibonacci from 15 Parallel");
        try {
            f.start();
        } finally {
            point.collect();
        }

        etmMonitor.render(new SimpleTextRenderer());
    }

    private static void sequenceText() {

        EtmPoint point = etmMonitor.createPoint("400x LoremIpsum API sequence");

        try {
            for (int i = 0; i < 10; i++) {
                URL url = new URL("https://loripsum.net/api/400/long");
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");
            }
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            point.collect();
        }
    }

    private static void parallelText() {
        EtmPoint point = etmMonitor.createPoint("400x LoremIpsum API parallel");
        try {
            for (int i = 0; i < 10; i++) {
                parralelTextThread thread = new parralelTextThread("parallel");
                thread.start();
            }
        } finally {
            point.collect();
        }
    }

    static class parralelTextThread implements Runnable {

        private Thread t;
        private String threadName;

        parralelTextThread(String name) {
            threadName = name;
        }

        public void run() {
            try {
                URL url = new URL("https://loripsum.net/api/400/long");
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        void start() {
            if (t == null) {
                t = new Thread(this, threadName);
                t.start();
            }
        }
    }

    private static void fibbonaciSequence(long n) {
        EtmPoint point = etmMonitor.createPoint("Fibonacci from 15 Sequence");
        try {
            fibonacci(n);
        } finally {
            point.collect();
        }
    }


    private static long fibonacci(long n) {
        if (n <= 1) return n;
        else return fibonacci(n - 1) + fibonacci(n - 2);
    }

    public static class Fib extends Thread {
        private int x;
        int answer;

        Fib(int x) {
            this.x = x;
        }

        public void run() {
            if (x <= 2)
                answer = 1;
            else {
                try {
                    Fib f1 = new Fib(x - 1);
                    Fib f2 = new Fib(x - 2);
                    f1.start();
                    f2.start();
                    f1.join();
                    f2.join();
                    answer = f1.answer + f2.answer;
                } catch (InterruptedException ignored) {
                }
            }
        }

        public static void main(String[] args) {
            try {
                Fib f = new Fib(Integer.parseInt(args[0]));
                f.start();
                f.join();
                System.out.println(f.answer);
            } catch (Exception e) {
                System.out.println("usage: java Fib NUMBER");
            }
        }
    }
}
