import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.net.URLEncoder;
import java.util.List;

public class scrap {


    @Test
    public void scraping() {
        String searchQuery = "Playstation" ;

        WebClient client = new WebClient();
        client.getOptions().setCssEnabled(false);
        client.getOptions().setJavaScriptEnabled(false);
        try {
            String searchUrl = "https://newyork.craigslist.org/search/sss?sort=rel&query=" + URLEncoder.encode(searchQuery, "UTF-8");
            System.out.println("Scrap from: "+ searchUrl);
            System.out.println("Searching: " + searchQuery);
            HtmlPage page = client.getPage(searchUrl);
            List<HtmlElement> items = (List<HtmlElement>) page.getByXPath("//p[@class='result-info']" );
            JSONArray result = new JSONArray();
            int id = 0;
            if(items.isEmpty()){
                System.out.println("No items found !");
            }else{
                for(HtmlElement item : items){
                    HtmlAnchor itemAnchor = item.getFirstByXPath(".//a");

                    String itemName = itemAnchor.asText();
                    String itemUrl = itemAnchor.getHrefAttribute() ;
                    HtmlElement spanPrice = item.getFirstByXPath(".//span[@class='result-price']") ;
                    // It is possible that an item doesn't have any price
                    String itemPrice = spanPrice == null ? "no price" : spanPrice.asText() ;

                    JSONObject oneitem = new JSONObject().put("name", itemName).put("Price", itemPrice).put("URL",itemUrl);
                    result.put(id, oneitem);
                    id++;
                }
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                String jsonString = gson.toJson(result);
                System.out.println(jsonString);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
